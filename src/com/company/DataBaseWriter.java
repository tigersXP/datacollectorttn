package com.company;

import java.sql.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Created by Kif Kroker on 12/1/2017.
 */
public class DataBaseWriter
{
    private Connection connection;

    /**
     * The constructor is responsible for creating a correct mysql connection.
     */
    public DataBaseWriter()
    {
        try
        {
            //todo read this from a file.
            connection = DriverManager.getConnection("jdbc:mysql://84.245.33.139/weather_data?autoReconnect=true", "weat", "9ev!GR");


            //make sure we are using the correct database
                Statement st = connection.createStatement();
                st.executeUpdate("use weather_data");
        }
        catch (SQLException e)
        {
            System.err.println("Error connecting to database");
            e.printStackTrace();
        }
    }

    /***
     * This creates a query from the string and executes is.
     * @param s quary string
     * @return succeeded
     */
    public boolean insertQuery(String s)
    {
        //System.out.print("\n[Performing INSERT] ... "+s);
        try
        {
            Statement st = connection.createStatement();
            return st.executeUpdate(s)>0;

        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    /**
     * Insert a predefined message into the database
     * @param dataMessage
     * @return returns true if the statement was executed.
     */
    public boolean insertDataMessage(DataMessage dataMessage)
    {
        System.out.println(dataMessage);
        return insertQuery("INSERT INTO weather_data.sensor_readings (Device, Temp, Humitity, Pressure, Luminosity, Time) " + "VALUES ('Primary Sensor', "+dataMessage.getTemp()+", "+dataMessage.getHum()+", "+dataMessage.getPressure()+", "+dataMessage.getLux()+", "+System.currentTimeMillis()/1000+")");
    }


}
