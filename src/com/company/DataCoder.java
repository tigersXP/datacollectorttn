package com.company;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created by Kif Kroker on 12/7/2017.
 */
public class DataCoder
{

    /**
     * This function doesn't actually encode the message but returns a literal payload but correct
     * @param dataIN -unused-
     * @return correct payload for the unit test
     */
    public byte[] encode(DataMessage dataIN)
    {
        return DatatypeConverter.parseHexBinary("0582010000186016BF4766E60B43");
    }

    /**
     * This function decodes the bytes of the payload into a correct DataMessage
     * @param dataIN of the payload.
     * @return The DataMessage containing the data.
     */
    public DataMessage decode(byte[] dataIN)
    {
        //05 FA 00 00 00 15    40 B9 C5 47    00 00 0C 42

        byte id = dataIN[0];
        //float temp1 = ByteBuffer.wrap(Arrays.copyOfRange(dataIN, 1, 1 + 4)).order(ByteOrder.LITTLE_ENDIAN).getFloat();

        byte[] temperaturePayload = Arrays.copyOfRange(dataIN, 1, 1 + 4);
        int temp = ByteBuffer.wrap(temperaturePayload).order(ByteOrder.BIG_ENDIAN.LITTLE_ENDIAN).getInt();
        //System.out.println(temp);

        if(temp>255)
        {
            temp = (temp-100)/10;
        }
        else
        {
            temp = temperaturePayload[0];
        }
        int hum = dataIN[5];
        float press = ByteBuffer.wrap(Arrays.copyOfRange(dataIN, 6, 6 + 4)).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        float lux = ByteBuffer.wrap(Arrays.copyOfRange(dataIN, 10, 10 + 4)).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        //float hum = ByteBuffer.wrap(Arrays.copyOfRange(dataIN, 1 + 4 + 4 + 4, 1 + 4 + 4 + 4 + 4)).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        //System.out.println("id "+id + "\n temp " + temp + "\n hum " + hum+ "\n press "+ press+ "\n lux " + lux);
        return new DataMessage(id, temp, lux, hum, press);
    }

}
