package com.company;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Kif Kroker on 12/7/2017.
 */
public class DataCollectorTTN
{
    public static void main(String[] args)
    {

        //Start the program in test mode
        if(args.length>0)
        {
            if(args[0].contains("test"))
            {
                //This starts the unit tests.
                UnitTest unitTest = new UnitTest();
                return;
            }
        }

        //This starts the actual program
        try {
            new DataReceiverClient(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
