package com.company;

/**
 * Created by Kif Kroker on 12/7/2017.
 */
public class DataMessage implements Comparable
{
    private byte id;
    private float temp;
    private float lux;
    private float hum;
    private float pressure;

    public DataMessage(byte id, float temp, float lux, float hum, float pressure)
    {
        this.id = id;
        this.temp = temp;
        this.lux = lux;
        this.hum = hum;
        this.pressure = pressure;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * <p>
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     * <p>
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     * <p>
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     * <p>
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     * <p>
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(Object o)
    {
        DataMessage input = (DataMessage)o;
        if((input.id==this.id && input.temp==this.temp && input.lux==this.lux && input.hum == this.hum && input.pressure == this.pressure))
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }


    @Override
    public String toString()
    {
        return "Message\n{\n \tid :"+id+"\n\ttemp :"+temp+"\n\tlux :"+lux+"\n\thum :"+hum+"\n\tpressure :"+pressure+"\n}";
    }

    public byte getId() {
        return id;
    }

    public float getTemp() {
        return temp;
    }

    public float getLux() {
        return lux;
    }

    public float getHum() {
        return hum;
    }

    public float getPressure() {
        return pressure;
    }
}
