package com.company;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.thethingsnetwork.data.common.Connection;
import org.thethingsnetwork.data.common.messages.*;
import org.thethingsnetwork.data.mqtt.Client;

import java.util.function.BiConsumer;

/**
 * Created by Kif Kroker on 12/1/2017.
 */
public class DataReceiverClient
{
    private static byte[] testReceivedPayload;
    private Client client;

    /**
     * This function will start the mqtt client and sets the handlers.
     * @param testing When this is true the receiver is in test mode (for the unit test) so messages won't be inserted into the database.
     * @return testReceivedPayload
     **/
    public DataReceiverClient(boolean testing) throws Exception
    {
        //Connection information to connect to our ttn application.
        String region = "eu";
        String appId = "lopy_weather_station";
        String accessKey = "ttn-account-v2._GgejECw4DYxyxOI7xJkgEmpfmoE57sEDMc-lSxSLkQ";

        //create client object so we can add handlers later
        client = new Client(region, appId, accessKey);

        //create dataBaseWriter object
        DataBaseWriter dataBaseWriter = new DataBaseWriter();

        DataCoder dataCoder = new DataCoder();

        //Create the main message handler
        BiConsumer handler = new BiConsumer() {
            @Override
            public void accept(Object o, Object o2)
            {
                //print out any received messages
                System.out.println("Received :"+o2+" from "+o);
                if(o.getClass()==String.class)
                {
                    //check if its actually from the tigersotaa application and if it's a uplink message
                    if(((String)o).equals("tigersotaa")&&(o2.getClass() == UplinkMessage.class))
                    {
                        if(!testing)
                        {
                            //digest message.
                            byte[] payload = (((UplinkMessage) o2).getPayloadRaw());
                            dataBaseWriter.insertDataMessage(dataCoder.decode(payload));

                        }
                        else
                        {
                            testReceivedPayload = (((UplinkMessage) o2).getPayloadRaw());
                        }
                    }

                }
            }
        };
        //Add this specific handler
        client.onMessage(handler);

        //Add onActiviation handler which prints out some basic data.
        client.onActivation((String _devId, ActivationMessage _data) -> System.out.println("Activation: " + _devId + ", data: " + _data.getDevAddr()));

        //Add onError handler which prints out some basic data.
        client.onError((Throwable _error) -> System.err.println("error: " + _error.getMessage()));

        //Add onCOnnected handler which prints we're connected.
        client.onConnected((Connection _client) -> System.out.println("connected !"));

        //Start listening
        client.start();
    }


    /**
     * This function is used for the unit test to retrieve the test payload
     * @return testReceivedPayload
     **/
    public byte[] getTestReceivedPayload() {
        return testReceivedPayload;
    }

    /**
     * This function closes the connection to ttn correctly.
     **/
    public void closeDataReceiver()
    {
        System.out.println("Closing client");
        try {
            client.end();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
