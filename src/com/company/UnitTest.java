package com.company;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.thoughtworks.selenium.*;
import org.apache.commons.codec.binary.Hex;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.thethingsnetwork.data.common.messages.UplinkMessage;
import sun.plugin2.message.Message;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Kif Kroker on 12/5/2017.
 */
public class UnitTest
{
    private final static String TEST_PAYLOAD = "0582010000186016BF4766E60B43";

    public UnitTest()
    {
        if(testAll())
        {
            System.out.println("All test passed!");
        }
        else
        {
            System.out.println("One or more test failed!");
        }
        System.exit(1);
    }

    /**
     * This function executes all tests and gives a global result
     * @return true if all tests passed, false if any test failed.
     */
    public boolean testAll()
    {
        boolean result = true;

        DataReceiverClient dataReceiverClient = null;
        //Start DataReceiverClient in test mode
        try
        {
            dataReceiverClient = new DataReceiverClient(true);
            Thread.sleep(2000);
        }
        catch (Exception e)
        {
            System.err.println("Test Failed to start DataReceiverClient");
            //e.printStackTrace();
            result = false;
        }


        //Send Simulate Payload
        if(simulatePayload() && dataReceiverClient!=null)
        {

            //Wait for the DataReceierClient Thread to process the payload
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Receive Simulated Payload
            System.out.println(Hex.encodeHexString((dataReceiverClient.getTestReceivedPayload())));
            if(Hex.encodeHexString((dataReceiverClient.getTestReceivedPayload())).equalsIgnoreCase(TEST_PAYLOAD) )
            {
                System.out.println("\nTest Passed! : Received correct payload\n");
            }
            else
            {
                System.err.println("\nTest Failed! : Received payload is not equal to Send payload\n");
                result = false;
            }

            DataMessage testMessage = new DataMessage((byte)6, 28f, 139.9f, 24f, 97836.8f);
            DataCoder dataCoder = new DataCoder();
            //Encode a test message
            byte[] encodedData = dataCoder.encode(testMessage);

            //Decode the test message
            DataMessage decodedMessage = dataCoder.decode(encodedData);

            //Compare the messages
            if(testMessage.equals(decodedMessage))
            {
                System.out.println(testMessage);
                System.out.println("\n"+decodedMessage);
                System.out.println("Test Passed! : Decoded Message is equal to testMassage\n");
            }
            else
            {
                System.out.println(testMessage);
                System.out.println("\n"+decodedMessage);
                System.err.println("Test Failed! : Decoded Message is not equal to testMessage\n");
                result = false;
            }



        }

        //Make connection to database
        //Create test quarry
        if(testMysqlConnection())
        {
            System.out.println("Test Passed! : Successful connection to database is made\n");
        }
        else
        {
            result = false;
            System.err.println("Test Failed! : Failed to connect to database\n");
        }


        dataReceiverClient.closeDataReceiver();
        return result;

    }


    private boolean testMysqlConnection()
    {
        //Make connection to database
        //Create test quarry
        try
        {

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://84.245.33.139/weather_data?autoReconnect=true", "weat", "9ev!GR");
            //String result = "Database connection success\n";
            Statement st = con.createStatement();
            st.executeQuery("select version()");
            return true;
        }
        catch(Exception exception)
        {
            return false;
        }
    }


    private boolean simulatePayload()
    {

        System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
        System.setProperty("webdriver.ie.logfile", "IEDriverServer.log");
        /**
         * Internet Options -> Security -> Check "Enable Protected Mode" for all zones (or disable for all zones)
         * Go to Advanced -> Security -> Check "Enable Enhanced Protected Mode"
         **/
        InternetExplorerDriver driver = new InternetExplorerDriver();


        String baseUrl = "https://console.thethingsnetwork.org/applications/lopy_weather_station/devices/tigersotaa";
        String expectedTitle = "The Things Network Console";
        String actualTitle = "";


        driver.get(baseUrl);


        //System.out.println(driver.getCurrentUrl());
        String url = driver.getCurrentUrl();

        int maxLoginAttempts = 45;

        while(url.contains("account.thethingsnetwork.org/users/login") && --maxLoginAttempts>0)
        {
            System.out.println("User not logged in");
            url = driver.getCurrentUrl();
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        if(maxLoginAttempts<1)
        {
            System.out.println("User failed to login");
            return false;
        }

        actualTitle = driver.getTitle();
        if(actualTitle.equals(expectedTitle)) {
            System.out.println("Console Opened");

            WebElement uplink = driver.findElementByName("payload");
            uplink.click();
            uplink.sendKeys(TEST_PAYLOAD);
            //uplink.sendKeys("0102030405");

            List<WebElement> el = driver.findElementsByCssSelector(".u1datDvKt0._2ZGsKUaDzP");

            boolean sendDownLink = true;

            for ( WebElement e : el )
            {
                if(sendDownLink)
                {
                    sendDownLink=false;
                }
                else
                {
                    JavascriptExecutor executor = (JavascriptExecutor)driver;
                    executor.executeScript("arguments[0].click();", e);
                    System.out.println("Simulated Uplink Payload");
                    System.out.println("Send :"+TEST_PAYLOAD);

                    //Wait for the javascript to be executed bevore we close the webpage
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        else
        {

            System.out.println("Test Failed Unexpected Title");
            return false;
        }

        //System.out.println("ActualTitle :"+actualTitle);

        driver.close();
        return true;
    }


}

